class Airplane:
    def __init__(self, name, seats_number):
        self.name = name
        self.seats_number = seats_number
        self.seats_taken = 0
        self.__mileage = 0

    def fly(self, distance):
        self.__mileage += distance

    def is_service_required(self):
        return self.__mileage > 10000

    def board_passengers(self, number_of_passengers):
        self.seats_taken += number_of_passengers
        if self.seats_taken > self.seats_number:
            self.seats_taken = self.seats_number

    def get_available_seats(self):
        return self.seats_number - self.seats_taken

    def get_mileage(self):
        return self.__mileage

    def __str__(self):
        return f"Samolot nazwa: {self.name}, ilość miejsc: {self.seats_number}, zajęte miejsca: {self.seats_taken}, przebieg: {self.__mileage}"

if __name__ == '__main__':
    airplane = Airplane("Airbus", 200)
    print(airplane)
    airplane.fly(30000)
    print(airplane)
    airplane.board_passengers(150)
    airplane.board_passengers(30)
    print(airplane)
    print(airplane.get_available_seats())
    print(airplane.is_service_required())
